"""
Created on Tue Jan 18 10:40:06 2022

@author: simone aka pax
"""

# import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import pathlib
from keras.applications.vgg16 import VGG16
from keras.applications.efficientnet import EfficientNetB7
from keras import layers, models
from tqdm import tqdm
from time import sleep
from keras.preprocessing import image

# from PIL import Image
import matplotlib.image as Image

IMG_SIZE = (208, 176)
IMG_SHAPE = IMG_SIZE + (3,)
STORE_DIR = "OutputProducts"
DATASET_DIR = "Alzheimer_Dataset"


def cnn_feature_extraction(cnn_type):

    # The code gives the option to both load a previously generated dataset or calculate one using a CNN of choice

    if cnn_type == "None":
        features_vmild = np.loadtxt(STORE_DIR + '/features_vmild.csv', delimiter=',')
        features_mode = np.loadtxt(STORE_DIR + '/features_moderate.csv', delimiter=',')
        features_nond = np.loadtxt(STORE_DIR + '/features_nondemented.csv', delimiter=',')
        #features_tot = np.loadtxt(STORE_DIR + '/features_tot.csv', delimiter=',')

    elif cnn_type == "VGG-16":
        #train_dir_very_mild = pathlib.Path("Alzheimer_Dataset/Balanced_Dataset/VeryMildDemented")
        #train_dir_moderate = pathlib.Path("Alzheimer_Dataset/Balanced_Dataset/ModerateDemented")
        #train_dir_non_dem = pathlib.Path("Alzheimer_Dataset/Balanced_Dataset/NonDemented")
        #train_dir_tot = pathlib.Path("Alzheimer_Dataset/Balanced_Dataset/TOT")

        #train_dataset_vmild = tf.keras.utils.image_dataset_from_directory(train_dir_very_mild, image_size=IMG_SIZE, shuffle=None)
        #train_dataset_mode = tf.keras.utils.image_dataset_from_directory(train_dir_moderate, batch_size=1, image_size=IMG_SIZE, shuffle=None)
        #train_dataset_nond = tf.keras.utils.image_dataset_from_directory(train_dir_non_dem, batch_size=1, image_size=IMG_SIZE, shuffle=None)
        #train_dataset_tot = tf.keras.utils.image_dataset_from_directory(train_dir_tot, batch_size=1, image_size=IMG_SIZE, shuffle=None)

        # PREFETCH SECTION
        # AUTOTUNE = tf.data.AUTOTUNE
        # train_dataset_vmild = train_dataset_vmild.prefetch(buffer_size=AUTOTUNE)
        # train_dataset_mode = train_dataset_mode.prefetch(buffer_size=AUTOTUNE)
        # train_dataset_nond = train_dataset_nond.prefetch(buffer_size=AUTOTUNE)

        #train_dataset_aggregated = []
        #for class_name in train_dataset_tot.class_names:
        #    train_dir_class = pathlib.Path("Alzheimer_Dataset/train/" + class_name)
        #    class_dataset = tf.keras.utils.image_dataset_from_directory(train_dir_class,
        #                                                                    batch_size=1,
        #                                                                    image_size=IMG_SIZE)
        #    train_dataset_aggregated.append(class_dataset)


        #image_dataset = []
        #for class_name in train_dir.iterdir():
        #    class_dir = pathlib.Path(class_name)
        #    class_dataset = []
        #    for file in class_dir.iterdir():
                #image = image.open(file)
        #        image = Image.imread(file)
        #        class_dataset.append(image)
        #    image_dataset.append(class_dataset)


        # DEFINE CNN layer by layer
        inputs = tf.keras.Input(shape=(208, 176, 3))

        # importo mobilenet v2 co default input shape (224,224) invece che (208,176)
        base_model = VGG16(weights="imagenet", include_top=False, input_shape=IMG_SHAPE)
        base_model.trainable = False

        #flatten_layer = layers.Flatten()
        pooling_layer = layers.GlobalMaxPooling2D()

        model = tf.keras.Sequential()
        model.add(inputs)
        model.add(base_model)
        #model.add(flatten_layer)
        model.add(pooling_layer)

        model.summary()

        vmild_path = pathlib.Path("Alzheimer_Dataset/Balanced_Dataset/Test/VeryMildDemented/")
        non_dem_path = pathlib.Path("Alzheimer_Dataset/Balanced_Dataset/Test/NonDemented/")
        moderate_path = pathlib.Path("Alzheimer_Dataset/Balanced_Dataset/Test/ModerateDemented/")

        v_mild_imgs = []
        non_dem_imgs = []
        moderate_imgs = []

        v_mild_file_n = []
        non_dem_file_n = []
        moderate_file_n = []

        for file in vmild_path.iterdir():
            img = image.load_img(file)
            x = image.img_to_array(img)
            x = np.expand_dims(x, axis=0)
            v_mild_imgs.append(x)
            v_mild_file_n.append(file.name)

        for file in non_dem_path.iterdir():
            img = image.load_img(file)
            x = image.img_to_array(img)
            x = np.expand_dims(x, axis=0)
            non_dem_imgs.append(x)
            non_dem_file_n.append(file.name)

        for file in moderate_path.iterdir():
            img = image.load_img(file)
            x = image.img_to_array(img)
            x = np.expand_dims(x, axis=0)
            moderate_imgs.append(x)
            moderate_file_n.append(file.name)

        features_nond = np.zeros([non_dem_imgs.__len__(), model.output_shape[1]+1])
        features_vmild = np.zeros([v_mild_imgs.__len__(), model.output_shape[1]+1])
        features_mode = np.zeros([moderate_imgs.__len__(), model.output_shape[1]+1])

        # Instantiating Dataset first column as idx
        # features_nond[:, 0] = np.arange(0, non_dem_imgs.__len__())
        # features_vmild[:, 0] = np.arange(0, v_mild_imgs.__len__())
        # features_mode[:, 0] = np.arange(0, moderate_imgs.__len__())

        # Assigning class belongings into the last column of the dataset named: Class
        features_nond[:, 512] = 1
        features_vmild[:, 512] = 2
        features_mode[:, 512] = 3

        header = ""
        for i in range(model.output_shape[1]):
            header += "DeepFeaut%d," % i
        header += "Class"

        print("\n\nFeatures extraction from image dataset using CNN in progress...")

        print("%s in progress..." % "VeryMild")
        sleep(1)
        for idx, img in tqdm(enumerate(v_mild_imgs)):
            features_vmild[idx][:512] = model.predict(img)
        np.save(STORE_DIR + '/features_vmild_test.npy', features_vmild)
        np.savetxt(STORE_DIR + '/features_vmild_test.csv', features_vmild, delimiter=',', header=header)
        print("Done!")
        sleep(1)

        print("%s in progress..." % "Moderate")
        sleep(1)
        for idx, img in tqdm(enumerate(moderate_imgs)):
            features_mode[idx][:512] = model.predict(img)
        np.save(STORE_DIR + '/features_moderate_test.npy', features_mode)
        np.savetxt(STORE_DIR + '/features_moderate_test.csv', features_mode, delimiter=',', header=header)
        print("Done!")
        sleep(1)

        print("%s in progress..." % "NonDemented")
        sleep(1)
        for idx, img in tqdm(enumerate(non_dem_imgs)):
            features_nond[idx][:512] = model.predict(img)
        np.save(STORE_DIR + '/features_nondemented_test.npy', features_nond)
        np.savetxt(STORE_DIR + '/features_nondemented_test.csv', features_nond, delimiter=',', header=header)
        print("Done!")
        sleep(1)

        #img_path2 = "Alzheimer_Dataset/Balanced_Dataset/VeryMildDemented/VeryMildDemented/verymildDem1.jpg"
        #img = image.load_img(img_path)
        #img2 = image.load_img(img_path2)
        #x = image.img_to_array(img)
        #y = image.img_to_array(img2)
        #x2 = np.expand_dims(x, axis=0)
        #x_features = model.predict(x2)

        # Put images as inputs of the cnn and retrieve the output features

        #features_vmild = np.zeros([len(train_dataset_vmild), model.output_shape[1]])
        #features_mode = np.zeros([len(train_dataset_mode), model.output_shape[1]])
        #features_nond = np.zeros([len(train_dataset_nond), model.output_shape[1]])

        #print("\n\nFeatures extraction from image dataset using CNN in progress...")

        #for class_name in train_dataset_tot.class_names:

        #print("%s in progress..." % "VeryMild")
        #for i in tqdm(range(train_dataset_vmild.__len__())):
            #    bottleneck_feature_example = model.predict(next(iter(train_dataset_vmild))[0])
            #features_vmild[i] = bottleneck_feature_example[0]
        #np.save(STORE_DIR + '/features_vmild.npy', features_vmild)
        #np.savetxt(STORE_DIR + '/features_vmild.csv', features_vmild, delimiter=',')
        #print("Done!")
        #sleep(1)

        #print("%s in progress..." % "Moderate")
        #for i in tqdm(range(train_dataset_mode.__len__())):
            #    bottleneck_feature_example = model.predict(next(iter(train_dataset_mode))[0])
            #features_mode[i] = bottleneck_feature_example[0]
        #np.save(STORE_DIR + '/features_moderate.npy', features_mode)
        #np.savetxt(STORE_DIR + '/features_moderate.csv', features_mode, delimiter=',')
        #print("Done!")
        #sleep(1)

        #print("%s in progress..." % "NonDemented")
        #for i in tqdm(range(train_dataset_nond.__len__())):
            #    bottleneck_feature_example = model.predict(next(iter(train_dataset_nond))[0])
            #features_nond[i] = bottleneck_feature_example[0]
        #np.save(STORE_DIR + '/features_nondemented.npy', features_nond)
        #np.savetxt(STORE_DIR + '/features_nondemented.csv', features_nond, delimiter=',')
        #print("Done!")
        #sleep(1)

    elif cnn_type == "EfficientNetB7":
        base_model = EfficientNetB7(include_top=False, weights="imagenet", input_shape=IMG_SHAPE)
        base_model.trainable = False
        inputs = tf.keras.Input(shape=(208, 176, 3))
        pooling_layer = layers.GlobalMaxPooling2D()

        model = tf.keras.Sequential()
        model.add(inputs)
        model.add(base_model)
        model.add(pooling_layer)
        model.summary()

        vmild_path = pathlib.Path("Alzheimer_Dataset/Balanced_Dataset/VeryMildDemented/VeryMildDemented/")
        non_dem_path = pathlib.Path("Alzheimer_Dataset/Balanced_Dataset/NonDemented/NonDemented/")
        moderate_path = pathlib.Path("Alzheimer_Dataset/Balanced_Dataset/ModerateDemented/ModerateDemented/")
        v_mild_imgs = []
        non_dem_imgs = []
        moderate_imgs = []

        v_mild_file_n = []
        non_dem_file_n = []
        moderate_file_n = []

        for file in vmild_path.iterdir():
            img = image.load_img(file)
            x = image.img_to_array(img)
            x = np.expand_dims(x, axis=0)
            v_mild_imgs.append(x)
            v_mild_file_n.append(file.name)

        for file in non_dem_path.iterdir():
            img = image.load_img(file)
            x = image.img_to_array(img)
            x = np.expand_dims(x, axis=0)
            non_dem_imgs.append(x)
            non_dem_file_n.append(file.name)

        for file in moderate_path.iterdir():
            img = image.load_img(file)
            x = image.img_to_array(img)
            x = np.expand_dims(x, axis=0)
            moderate_imgs.append(x)
            moderate_file_n.append(file.name)

        features_nond = np.zeros([non_dem_imgs.__len__(), model.output_shape[1] + 1])
        features_vmild = np.zeros([v_mild_imgs.__len__(), model.output_shape[1] + 1])
        features_mode = np.zeros([moderate_imgs.__len__(), model.output_shape[1] + 1])

        features_nond[:, 2560] = 1
        features_vmild[:, 2560] = 2
        features_mode[:, 2560] = 3
        header = ""
        for i in range(model.output_shape[1]):
            header += "DeepFeaut%d," % i
        header += "Class"

        print("\n\nFeatures extraction from image dataset using CNN in progress...")

        # STORE_DIR = "OutputProducts"
        DATASET_DIR = "Alzheimer_Dataset"

        print("%s in progress..." % "VeryMild")
        sleep(1)
        for idx, img in tqdm(enumerate(v_mild_imgs)):
            features_vmild[idx][:2560] = model.predict(img)
        np.save(STORE_DIR + '/features_vmild_E7Net.npy', features_vmild)
        np.savetxt(STORE_DIR + '/features_vmild_E7Net.csv', features_vmild, delimiter=',', header=header)
        print("Done!")
        sleep(1)

        print("%s in progress..." % "Moderate")
        sleep(1)
        for idx, img in tqdm(enumerate(moderate_imgs)):
            features_mode[idx][:2560] = model.predict(img)
        np.save(STORE_DIR + '/features_moderate_E7Net.npy', features_mode)
        np.savetxt(STORE_DIR + '/features_moderate_E7Net.csv', features_mode, delimiter=',', header=header)
        print("Done!")
        sleep(1)

        print("%s in progress..." % "NonDemented")
        sleep(1)
        for idx, img in tqdm(enumerate(non_dem_imgs)):
            features_nond[idx][:2560] = model.predict(img)
        np.save(STORE_DIR + '/features_nondemented_E7Net.npy', features_nond)
        np.savetxt(STORE_DIR + '/features_nondemented_E7Net.csv', features_nond, delimiter=',', header=header)
        print("Done!")
        sleep(1)

    features_tot = [features_vmild, features_mode, features_nond]

    return features_tot
