import numpy as np

from parser1 import parse
from cnn import cnn_feature_extraction
from data_handling import load_img_dataset, plot_results, select_dataset_with_features, load_aggregated_data
from classifier import feature_selection_3class, train_classifier, test_classifier, feature_selection_2class, train_pls_da
from data_handling import save_features, load_features, select_dataset_with_features_plsda,load_all_features_and_get_uniques, retrieve_tot_dataset


def quick_run_iters():
    for j in range(3):
        for i in range(10):
            print("Run: %d %d" % (j, i))
            idx = j
            features_class2 = load_features(idx)
            dataset_aggregated_train = load_aggregated_data(idx, "train")
            dataset_aggregated_test = load_aggregated_data(idx, "test")
            X_train, X_test, Y_train, Y_test = select_dataset_with_features(features_class2, dataset_aggregated_train)

            x_test_opt, y_test_opt = dataset_aggregated_test.drop('Class', axis=1), dataset_aggregated_test['Class']
            x_test_opt = x_test_opt[features_class2]

            train_classifier("LDA", X_train, X_test, Y_train, Y_test, x_test_opt, y_test_opt)
            print("")


def quick_iter_pls_da(idx):
    for i in range(idx):
        features_tot, feaut_unique = load_all_features_and_get_uniques()
        x_train_1, y_train_1, x_train_2, y_train_2, x_train_3, y_train_3, x_test, y_test = select_dataset_with_features_plsda(np.array([1]))
        print("Iter:%d" % i)
        print("\tTrain shape %d %d" % x_train_1.shape, "Test shape %d %d" % x_test.shape)
        train_pls_da(x_train_1, y_train_1, x_train_2, y_train_2, x_train_3, y_train_3, x_test, y_test)
        print("")


IMG_DATASET_FOLDER = "img_dataset_folder"
OUTPUT_PRODUCTS = "dataset_folder"

#parametri iniziali
opts_cnn = "None"  # VGG-16 EfficientNetB7 None
opts_classifier = "LDA"  # LDA PLS-DA FFNET
opts_feature_sel = "Stored"  # OvsO OvsA Stored None

# options = parse()
print("CNN:", opts_cnn, " Classifier:", opts_classifier, " Feaut sel:", opts_feature_sel)

# access the dataset folder and load images as separated classes
load_img_dataset()

# extract features from the images and store them as separate csv file
dataset = cnn_feature_extraction(opts_cnn)

# perform feature selection using the AUC on previously extracted features
if opts_feature_sel == "OvsO":
    features_class1, features_class2, features_class3, dataset_aggregated = feature_selection_2class()
    features = [features_class1, features_class2, features_class3]
    save_features(features, opts_feature_sel)
    features_tot, feaut_unique = load_all_features_and_get_uniques()

elif opts_feature_sel == "OvsA":
    features_class1, features_class2, features_class3, dataset_aggregated = feature_selection_3class()
    features = [features_class1, features_class2, features_class3]
    save_features(features, opts_feature_sel)
    features_tot, feaut_unique = load_all_features_and_get_uniques()

elif opts_feature_sel == "Stored":
    print("No feature sel performed resorting to loading stored features")
    dataset_aggregated = retrieve_tot_dataset()
    features_tot, feaut_unique = load_all_features_and_get_uniques()

else:
    print("No feature sel performed resorting to using all the dataset")
    dataset_aggregated = retrieve_tot_dataset()
    feaut_unique = np.array([1])

X_train, X_test, Y_train, Y_test = select_dataset_with_features(feaut_unique)
x_train_1, y_train_1, x_train_2, y_train_2, x_train_3, y_train_3, x_test, y_test = select_dataset_with_features_plsda(feaut_unique)

# classifier train
if opts_classifier == "PLS-DA":
    train_pls_da(x_train_1, y_train_1, x_train_2, y_train_2, x_train_3, y_train_3, x_test, y_test)
elif opts_classifier == "LDA":
    train_classifier("LDA", X_train, X_test, Y_train, Y_test)
#quick_run_iters()
#quick_iter_pls_da(2)






# classifier test
# test_classifier(opts_classifier)

# plot results and store data (classifier, extracted features, results report)
# plot_results()

# UNCOMMENT FOR QUICK ACCESS CODE
# save_features(features)


