import numpy as np
from sklearn.model_selection import train_test_split
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from classifier import rand_balanced_dataset, rand_balanced_dataset_2_classi
from sklearn.utils import shuffle


def load_img_dataset():
    a = 5


def plot_results():
    a = 5


def save_features(features_tot, type):
    for idx, features in enumerate(features_tot):
        np.save("OutputProducts/features_classifier%d_%s.npy" % (idx, type), features)
    print("Securely stored the important obtained features %s" % type)


def load_features(idx, type):
    features = np.load("OutputProducts/features_classifier%d_%s.npy" % (idx, type))
    return features


def load_all_auc_scores():
    auc_scores_1 = pd.read_pickle("OutputProducts/auc_scores_1vsa.pkl")
    auc_scores_2 = pd.read_pickle("OutputProducts/auc_scores_2vsa.pkl")
    auc_scores_3 = pd.read_pickle("OutputProducts/auc_scores_3vsa.pkl")
    auc_scores_4 = pd.read_pickle("OutputProducts/auc_scores_1vs2.pkl")
    auc_scores_5 = pd.read_pickle("OutputProducts/auc_scores_2vs3.pkl")
    auc_scores_6 = pd.read_pickle("OutputProducts/auc_scores_3vs1.pkl")

    auc_scores_tot = [auc_scores_1, auc_scores_2, auc_scores_3, auc_scores_4, auc_scores_5, auc_scores_6]

    return auc_scores_tot


def load_all_features_and_get_uniques():
    feature1 = load_features(0, "OvsO")
    feature2 = load_features(1, "OvsO")
    feature3 = load_features(2, "OvsO")
    feature4 = load_features(0, "OvsA")
    feature5 = load_features(1, "OvsA")
    feature6 = load_features(2, "OvsA")

    feaut_tot = [feature1, feature2, feature3, feature4, feature5, feature6]
    feaut_unique_low = np.append(feature1, feature2)
    feaut_unique_medium = np.append(feature3, feature4)
    feaut_unique_high = np.append(feature5, feature6)
    feaut_unique_pre = np.append(feaut_unique_low, feaut_unique_medium)

    feaut_unique_np = np.append(feaut_unique_high, feaut_unique_pre)

    feaut_unique_panda = pd.Series(feaut_unique_np)
    duplicated_features = feaut_unique_panda.duplicated()

    features_to_keep = [not index for index in duplicated_features]

    feaut_unique = np.array(feaut_unique_panda[features_to_keep])
    np.save("OutputProducts/features_classifier_unique.npy", feaut_unique)

    return feaut_tot, feaut_unique


def retrieve_tot_dataset():
    data_non = pd.read_csv('OutputProducts/features_nondemented.csv')  # , nrows=20000)
    data_vmild = pd.read_csv('OutputProducts/features_vmild.csv')  # , nrows=20000)
    data_moderate = pd.read_csv('OutputProducts/features_moderate.csv')  # , nrows=20000)
    trans = MinMaxScaler()
    data_non = pd.DataFrame(trans.fit_transform(data_non)).rename(columns={512: 'Class'}).assign(Class=1)
    data_vmild = pd.DataFrame(trans.fit_transform(data_vmild)).rename(columns={512: 'Class'}).assign(Class=2)
    data_moderate = pd.DataFrame(trans.fit_transform(data_moderate)).rename(columns={512: 'Class'}).assign(Class=3)

    aggregated_data = pd.concat([data_non, data_vmild, data_moderate], ignore_index=True)

    return aggregated_data


def select_dataset_with_features_plsda(features_class):

    data_non = pd.read_csv('OutputProducts/features_nondemented.csv')  # , nrows=20000)
    data_vmild = pd.read_csv('OutputProducts/features_vmild.csv')  # , nrows=20000)
    data_moderate = pd.read_csv('OutputProducts/features_moderate.csv')  # , nrows=20000)

    trans = MinMaxScaler()

    # perform a robust scaler transform of the dataset
    data_non = pd.DataFrame(trans.fit_transform(data_non)).rename(columns={512: 'Class'}).assign(Class=1)
    data_vmild = pd.DataFrame(trans.fit_transform(data_vmild)).rename(columns={512: 'Class'}).assign(Class=2)
    data_moderate = pd.DataFrame(trans.fit_transform(data_moderate)).rename(columns={512: 'Class'}).assign(Class=3)

    train_prcntg = 0.5

    data_non_shuf = shuffle(data_non)
    data_vmild_shuf = shuffle(data_vmild)
    data_moderate_shuf = shuffle(data_moderate)

    idx1 = int(train_prcntg*data_non_shuf.__len__())
    idx2 = int(train_prcntg*data_vmild_shuf.__len__())
    idx3 = int(train_prcntg*data_moderate_shuf.__len__())

    data_non_train = data_non_shuf[0:idx1]
    data_non_test = data_non_shuf[idx1: data_non_shuf.__len__()]
    data_vmild_train = data_vmild_shuf[0:idx2]
    data_vmild_test = data_vmild_shuf[idx2: data_vmild_shuf.__len__()]
    data_moderate_train = data_moderate_shuf[0:idx3]
    data_moderate_test = data_moderate_shuf[idx3: data_moderate_shuf.__len__()]


    data_1 = rand_balanced_dataset(data_non_train, data_vmild_train, data_moderate_train, 0)  # 1vsa
    data_2 = rand_balanced_dataset(data_non_train, data_vmild_train, data_moderate_train, 1)  # 2vsa
    data_3 = rand_balanced_dataset(data_non_train, data_vmild_train, data_moderate_train, 2)  # 3vsa

    x_train_1 = data_1.drop('Class', axis=1)
    x_train_2 = data_2.drop('Class', axis=1)
    x_train_3 = data_3.drop('Class', axis=1)

    y_train_1 = data_1['Class']
    y_train_2 = data_2['Class']
    y_train_3 = data_3['Class']

    data_test = shuffle(pd.concat([data_non_test, data_vmild_test, data_moderate_test]))

    x_test = data_test.drop('Class', axis=1)
    y_test = data_test['Class']

    if not (features_class.__len__() == 1):
        x_train_1 = x_train_1[features_class]
        x_train_2 = x_train_2[features_class]
        x_train_3 = x_train_3[features_class]
        x_test = x_test[features_class]

    return x_train_1, y_train_1, x_train_2, y_train_2, x_train_3, y_train_3, x_test, y_test


def select_dataset_with_features(selected_features):

    train_prcntg = 0.5

    data_non = pd.read_csv('OutputProducts/features_nondemented.csv')  # , nrows=20000)
    data_vmild = pd.read_csv('OutputProducts/features_vmild.csv')  # , nrows=20000)
    data_moderate = pd.read_csv('OutputProducts/features_moderate.csv')  # , nrows=20000)

    trans = MinMaxScaler()

    # perform a robust scaler transform of the dataset
    data_non = pd.DataFrame(trans.fit_transform(data_non)).rename(columns={512: 'Class'}).assign(Class=1)
    data_vmild = pd.DataFrame(trans.fit_transform(data_vmild)).rename(columns={512: 'Class'}).assign(Class=2)
    data_moderate = pd.DataFrame(trans.fit_transform(data_moderate)).rename(columns={512: 'Class'}).assign(Class=3)

    dataset = pd.concat([data_non, data_vmild, data_moderate])

    X = dataset.drop('Class', axis=1)
    y = dataset['Class']

    if not selected_features.__len__() == 1:
        X = X[selected_features]

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=train_prcntg, random_state=0, stratify=y)

    return X_train, X_test, y_train, y_test


def load_aggregated_data(idx, data_flag):

    if data_flag == "train":
        ln = ""
    elif data_flag == "test":
        ln = "_test"
    else:
        ln = ""

    data_non = pd.read_csv('OutputProducts/features_nondemented%s.csv' % ln)  # , nrows=20000)
    data_vmild = pd.read_csv('OutputProducts/features_vmild%s.csv' %ln)  # , nrows=20000)
    data_moderate = pd.read_csv('OutputProducts/features_moderate%s.csv' %ln)  # , nrows=20000)

    trans = MinMaxScaler()

    # perform a robust scaler transform of the dataset
    # TODO autoscale with STANDARDIZATION
    data_non = pd.DataFrame(trans.fit_transform(data_non)).rename(columns={512: 'Class'}).assign(Class=1)
    data_vmild = pd.DataFrame(trans.fit_transform(data_vmild)).rename(columns={512: 'Class'}).assign(Class=2)
    data_moderate = pd.DataFrame(trans.fit_transform(data_moderate)).rename(columns={512: 'Class'}).assign(Class=3)

    aggregated_data = pd.concat([data_non, data_vmild, data_moderate], ignore_index=True)
    # aggregated_data = rand_balanced_dataset(data_non, data_vmild, data_moderate, idx)
    return aggregated_data



