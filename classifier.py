import sys

import numpy as np
import pandas as pd
# import matplotlib.pyplot as plt
import multiprocessing as mp
import seaborn as sns

import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

import random

from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, roc_auc_score
from sklearn.feature_selection import VarianceThreshold
from sklearn.cross_decomposition import PLSRegression
from sklearn.preprocessing import MinMaxScaler
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.metrics import accuracy_score
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.model_selection import cross_val_score
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.model_selection import RepeatedKFold
from sklearn.preprocessing import scale
sns.set()

CLASSES = ["NonDemented", "VMild", "Moderate"]


def feature_selection_2class():
    # this feature selection is used in the ovo (1vs2, 2vs3, 3vs1) classifiers
    data_non = pd.read_csv('OutputProducts/features_nondemented.csv')  # , nrows=20000)
    data_vmild = pd.read_csv('OutputProducts/features_vmild.csv')  # , nrows=20000)
    data_moderate = pd.read_csv('OutputProducts/features_moderate.csv')  # , nrows=20000)

    trans = MinMaxScaler()

    # perform a robust scaler transform of the dataset
    # TODO autoscale with STANDARDIZATION
    data_non = pd.DataFrame(trans.fit_transform(data_non)).rename(columns={512: 'Class'}).assign(Class=1)
    data_vmild = pd.DataFrame(trans.fit_transform(data_vmild)).rename(columns={512: 'Class'}).assign(Class=2)
    data_moderate = pd.DataFrame(trans.fit_transform(data_moderate)).rename(columns={512: 'Class'}).assign(Class=3)

    aggregated_data = pd.concat([data_non, data_vmild, data_moderate], ignore_index=True)

    data_non_v, data_vmild_v, data_moderate_v = preliminary_feature_sel(data_non, data_vmild, data_moderate)

    # Given:
    # 1: Non Demented
    # 2: Very Mild Demented
    # 3: Moderate Demented
    data_bal_tot_1 = []  # associated to the 1vs2 classifier
    data_bal_tot_2 = []  # associated to the 2vs3 classifier
    data_bal_tot_3 = []  # associated to the 3vs1 classifier

    iterations = 100

    for i in range(iterations):
        data_bal_tot_1.append(rand_balanced_dataset_2_classi(data_non_v, data_vmild_v))
        data_bal_tot_2.append(rand_balanced_dataset_2_classi(data_vmild_v, data_moderate_v))
        data_bal_tot_3.append(rand_balanced_dataset_2_classi(data_moderate_v, data_non_v))


    pool = mp.Pool(processes=mp.cpu_count() - 1)
    results1 = [pool.apply_async(feature_selection, args=(data_bal,)) for data_bal in data_bal_tot_1] # maps function to iterator
    output1 = [p.get() for p in results1]   # collects and returns the results

    results2 = [pool.apply_async(feature_selection, args=(data_bal,)) for data_bal in data_bal_tot_2]  # maps function to iterator
    output2 = [p.get() for p in results2]  # collects and returns the results

    results3 = [pool.apply_async(feature_selection, args=(data_bal,)) for data_bal in data_bal_tot_3]  # maps function to iterator
    output3 = [p.get() for p in results3]  # collects and returns the results


    auc_scores_1 = output1
    auc_scores_2 = output2
    auc_scores_3 = output3

    auc_scores_1 = pd.DataFrame(auc_scores_1)
    auc_scores_2 = pd.DataFrame(auc_scores_2)
    auc_scores_3 = pd.DataFrame(auc_scores_3)

    # SAVE EXTRACTED AUC of FEATURES
    auc_scores_1.to_pickle("OutputProducts/auc_scores_1vs2.pkl")
    auc_scores_2.to_pickle("OutputProducts/auc_scores_2vs3.pkl")
    auc_scores_3.to_pickle("OutputProducts/auc_scores_3vs1.pkl")

    # auc_scores_3 = pd.read_pickle("OutputProducts/auc_scores_3vsa.pkl")

    features_class1 = evaluate_features_on_best_medians(auc_scores_1, "AUC scores 1vs2")
    features_class2 = evaluate_features_on_best_medians(auc_scores_2, "AUC scores 2vs3")
    features_class3 = evaluate_features_on_best_medians(auc_scores_3, "AUC scores 3vs1")

    # once the features have been extracted for every classifier, we proceed to train classifiers with them

    # return auc_scores_1, auc_scores_2, auc_scores_3
    return features_class1, features_class2, features_class3, aggregated_data


def feature_selection_3class():
    data_non = pd.read_csv('OutputProducts/features_nondemented.csv')  # , nrows=20000)
    data_vmild = pd.read_csv('OutputProducts/features_vmild.csv')  # , nrows=20000)
    data_moderate = pd.read_csv('OutputProducts/features_moderate.csv')  # , nrows=20000)

    trans = MinMaxScaler()

    # perform a robust scaler transform of the dataset
    # TODO autoscale with STANDARDIZATION
    data_non = pd.DataFrame(trans.fit_transform(data_non)).rename(columns={512: 'Class'}).assign(Class=1)
    data_vmild = pd.DataFrame(trans.fit_transform(data_vmild)).rename(columns={512: 'Class'}).assign(Class=2)
    data_moderate = pd.DataFrame(trans.fit_transform(data_moderate)).rename(columns={512: 'Class'}).assign(Class=3)

    aggregated_data = pd.concat([data_non, data_vmild, data_moderate], ignore_index=True)

    # A preliminary feature selection is based on FEATURES VARIANCE.
    # We remove the ones with low variance
    data_non_v, data_vmild_v, data_moderate_v = preliminary_feature_sel(data_non, data_vmild, data_moderate)

    auc_scores_1, auc_scores_2, auc_scores_3 = [], [], []
    auc_scores = [auc_scores_1, auc_scores_2, auc_scores_3]
    # iterate for every classifier several feature selection runs and choose features for each one

    data_bal_tot_1, data_bal_tot_2, data_bal_tot_3 = [], [], []

    iterations = 100

    for i in range(iterations):
        data_bal_tot_1.append(rand_balanced_dataset(data_non_v, data_vmild_v, data_moderate_v, 0))
        data_bal_tot_2.append(rand_balanced_dataset(data_non_v, data_vmild_v, data_moderate_v, 1))
        data_bal_tot_3.append(rand_balanced_dataset(data_non_v, data_vmild_v, data_moderate_v, 2))

    pool = mp.Pool(processes=mp.cpu_count() - 1)
    results1 = [pool.apply_async(feature_selection, args=(data_bal,)) for data_bal in data_bal_tot_1] # maps function to iterator
    output1 = [p.get() for p in results1]   # collects and returns the results

    results2 = [pool.apply_async(feature_selection, args=(data_bal,)) for data_bal in data_bal_tot_2]  # maps function to iterator
    output2 = [p.get() for p in results2]  # collects and returns the results

    results3 = [pool.apply_async(feature_selection, args=(data_bal,)) for data_bal in data_bal_tot_3]  # maps function to iterator
    output3 = [p.get() for p in results3]  # collects and returns the results

    auc_scores_1 = output1
    auc_scores_2 = output2
    auc_scores_3 = output3

    # for chosen_class in range(3):
    #     for run in range(100):
            # TODO 2 perform feature selection for 1vs2 2vs3 3vs1
    #         data_balanced = rand_balanced_dataset(data_non_v, data_vmild_v, data_moderate_v, chosen_class)
    #         auc_score = feature_selection(data_balanced)

    #         auc_scores[chosen_class].append(auc_score)
    #         print("\nIter: %d %d" % (chosen_class, run))
            # print(data_balanced.head())
            # print(data_balanced.describe())

    auc_scores_1 = pd.DataFrame(auc_scores_1)
    auc_scores_2 = pd.DataFrame(auc_scores_2)
    auc_scores_3 = pd.DataFrame(auc_scores_3)

    # SAVE EXTRACTED AUC of FEATURES
    auc_scores_1.to_pickle("OutputProducts/auc_scores_1vsa.pkl")
    auc_scores_2.to_pickle("OutputProducts/auc_scores_2vsa.pkl")
    auc_scores_3.to_pickle("OutputProducts/auc_scores_3vsa.pkl")

    # auc_scores_3 = pd.read_pickle("OutputProducts/auc_scores_3vsa.pkl")

    features_class1 = evaluate_features_on_best_medians(auc_scores_1, "AUC_scores_1vsall")
    features_class2 = evaluate_features_on_best_medians(auc_scores_2, "AUC_scores_2vsall")
    features_class3 = evaluate_features_on_best_medians(auc_scores_3, "AUC_scores_3vsall")

    # once the features have been extracted for every classifier, we proceed to train classifiers with them

    # return auc_scores_1, auc_scores_2, auc_scores_3
    return features_class1, features_class2, features_class3, aggregated_data


def evaluate_features_on_best_medians(auc_dataframe, title):

    selected_features = 20  # TODO 40
    box_len_threshold = 0.1

    plt.figure()
    plt.title(title)
    auc_dataframe.boxplot()

    box_info = auc_dataframe.quantile([0.1, 0.5, 0.9]).transpose()

    median = box_info[0.5]

    a = median.copy()
    a.sort_values(ascending=False, inplace=True)
    idx = a.index

    feautures_median_ordered = auc_dataframe[idx]
    plt.figure()
    plt.title(title + " median ordered")
    feautures_median_ordered.boxplot()
    best_features = feautures_median_ordered.T[0:selected_features-1].T

    plt.figure()
    plt.title(title + " chosen features")
    best_features.boxplot()

    return best_features.columns


def evaluate_box_plot_and_extract_features(auc_dataframe, title):
    plt.figure()
    plt.title(title)
    auc_dataframe.boxplot()

    median_threshold = 0.55
    box_len_threshold = 0.1

    # calcola la mediana
    # Q1 e Q3
    # threshold tra Q3-Q1
    box_info = auc_dataframe.quantile([0.1, 0.5, 0.9]).transpose()

    whisk1 = box_info[0.1]
    whisk3 = box_info[0.9]
    median = box_info[0.5]

    box_length = whisk3 - whisk1
    box_threshold_idx = box_length < box_len_threshold
    features_to_keep_1 = [index for index in box_threshold_idx]

    median_threshold_idx = median > median_threshold
    features_to_keep_2 = [idx for idx in median_threshold_idx]

    auc_selected = auc_dataframe.transpose()[features_to_keep_1 and features_to_keep_2].transpose()
    ret = 0
    if not auc_selected.empty:
        plt.figure()
        plt.title(title)
        auc_selected.boxplot()
        ret = auc_selected.columns
    # TODO add to the boxplot the worst features
    return ret


def preliminary_feature_sel(data1, data2, data3):

    # data = pd.concat([data1, data2, data3])
    data = pd.concat([data1, data2, data3], ignore_index=True)
    l1, l2, l3 = data1.__len__(), data2.__len__(), data3.__len__()
    X = data.drop('Class', axis=1)
    y = data['Class']

    constant_filter = VarianceThreshold(threshold=0.01)
    constant_filter.fit(X)
    X_filtered = constant_filter.transform(X)
    print(X_filtered.shape)
    X_filtered_T = X_filtered.T

    X_filtered_T = pd.DataFrame(X_filtered_T)

    X_filtered_T.duplicated().sum()
    duplicated_features = X_filtered_T.duplicated()
    features_to_keep = [not index for index in duplicated_features]
    X_unique = X_filtered_T[features_to_keep].T

    print(X_unique.shape)
    print(X.shape)

    #increase dataset of a small offset
    X_unique += 0.0000001
    # reassign classes
    X_unique['Class'] = y

    data_non_v = X_unique.iloc[range(l1)]
    data_vmild_v = X_unique.iloc[range(l1, l1+l2)]
    data_moderate_v = X_unique.iloc[range(l2, l2+l3)]

    return data_non_v, data_vmild_v, data_moderate_v


def feature_selection(dataset):
    # data = pd.read_csv('OutputProducts/features_moderate.csv')  # , nrows=20000)

    # data_non = pd.read_csv('OutputProducts/features_nondemented.csv')  # , nrows=20000)
    # data_vmild = pd.read_csv('OutputProducts/features_vmild.csv')  # , nrows=20000)
    # data_moderate = pd.read_csv('OutputProducts/features_moderate.csv')  # , nrows=20000)
    # makes more sense to iterate the function "feature selection" giving as input only data_rand already balanced
    # and externally call it in a for cycle

    # randomize and balance the data choosing between two of them
    # data_rand = __rand_balanced_dataset(data_vmild, data_moderate)

    data = dataset
    # print(data.head())
    # print(data.describe())

    # X = data.drop('TARGET', axis=1)
    # y = data['TARGET']
    X = data.drop('Class', axis=1)
    y = data['Class']


    X.shape, y.shape

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, stratify=y)

    # remove constant and quasi constant features
    # constant_filter = VarianceThreshold(threshold=0.01)
    # constant_filter.fit(X_train)
    # X_train_filter = constant_filter.transform(X_train)
    # X_test_filter = constant_filter.transform(X_test)

    # X_train_filter.shape, X_test_filter.shape
    # X_train_T = X_train_filter.T
    # X_test_T = X_test_filter.T

    # X_train.shape, X_test.shape
    # X_train_T = X_train.T
    # X_test_T = X_test.T

    # X_train_T = pd.DataFrame(X_train_T)
    # X_test_T = pd.DataFrame(X_test_T)

    # X_train_T.duplicated().sum()
    # duplicated_features = X_train_T.duplicated()

    # features_to_keep = [not index for index in duplicated_features]

    # X_train_unique = X_train_T[features_to_keep].T
    # X_test_unique = X_test_T[features_to_keep].T

    # X_train_unique.shape, X_train.shape

    roc_auc = []
    for feature in X_train.columns:
        # print(feature)
        # clf = RandomForestClassifier(n_estimators=50, random_state=0)
        clf = PLSRegression(n_components=1)
        clf.fit(X_train[feature].to_frame(), y_train)
        y_pred = clf.predict(X_test[feature].to_frame())
        roc_auc.append(roc_auc_score(y_test, y_pred))

    # print(roc_auc)

    roc_values = pd.Series(roc_auc)
    roc_values.index = X_train.columns
    roc_values.sort_values(ascending=False, inplace=True)
    roc_values
    roc_values.plot.bar()
    sel = roc_values[roc_values > 0.5]
    sel2 = roc_values[roc_values > 0.55]
    sel3 = roc_values[roc_values > 0.6]
    print(sel.__len__())
    print(sel2.__len__())
    print(sel3.__len__())
    X_train_roc = X_train[sel3.index]
    X_test_roc = X_test[sel3.index]

    return roc_values
    # return sel3 , X_train_roc, X_test_roc, y_train, y_test, roc_values


def rand_balanced_dataset(data1, data2, data3, chosen_data):

    # select which of the 2 dataset has a lower amount of entries
    # print("Accounting for the datasets with following dimensions:\n"
    #      "\t1: %drowsx%dcols" % data1.shape+" 2: %drowsx%dcols" % data2.shape+" 3: %drowsx%dcols\n" % data3.shape)

    if chosen_data == 0:
        data_main = data1
        data_all = [data2, data3]
    elif chosen_data == 1:
        data_main = data2
        data_all = [data1, data3]
    elif chosen_data == 2:
        data_main = data3
        data_all = [data1, data2]

    data_len = np.min([data1.__len__(), data2.__len__(), data3.__len__()])

    samples_idx_main = random.sample(range(data_main.__len__()), data_len)
    samples_idx_other1 = random.sample(range(data_all[0].__len__()), int(data_len/2))
    samples_idx_other2 = random.sample(range(data_all[1].__len__()), int(data_len/2))

    # select the subdataset specifying the rows from a list of indexes
    subset_data_main = data_main.iloc[samples_idx_main]
    subset_data_other1 = data_all[0].iloc[samples_idx_other1]
    subset_data_other2 = data_all[1].iloc[samples_idx_other2]

    subset_data_other = pd.concat([subset_data_other1, subset_data_other2])

    subset_data_other = subset_data_other.assign(Class=0)
    subset_data_main = subset_data_main.assign(Class=1)

    data_rand = pd.concat([subset_data_other, subset_data_main])
    # print("\nBalanced rand datset dimensions:\n"
    #       "\t%drowsx%dcols" % data_rand.shape)

    return data_rand


def rand_balanced_dataset_2_classi(data1, data2):

    # select which of the 2 dataset has a lower amount of entries
    l1 = data1.__len__()
    l2 = data2.__len__()
    # print("Accounting for the datasets with following dimensions:\n"
    #      "\t1: %drowsx%dcols" % data1.shape+" 2: %drowsx%dcols\n" % data2.shape)

    data1 = data1.assign(Class=1)
    data2 = data2.assign(Class=0)

    if l1 > l2:
        min_len = l2
        max_len = l1
        data_smol = data2
        data_big = data1
    else:
        min_len = l1
        max_len = l2
        data_smol = data1
        data_big = data2

    # return a list of random non repeating indexes
    # this way we select a subset of the bigger dataset
    # random.sample(50,5) -- returns 5 non repeating ints between 0 and 49
    subset = random.sample(range(max_len), min_len)
    #print("chosen subdataset idxs are as follows\n", subset)

    # select the subdataset specifying the rows from a list of indexes
    subdataset = data_big.iloc[subset]

    # aggregate 2 different datasets keeping columns of both
    # ex a = 512colsx100rows, b = 512colsx200rows. concat([a, b]) = 512colsx300rows
    data_rand = pd.concat([data_smol, subdataset])
    # print("\nBalanced rand datset dimensions:\n"
    #      "\t%drowsx%dcols" % data_rand.shape)

    return data_rand


def train_classifier(classifier, x_train, x_test, y_train, y_test):
    if classifier == "PLS-DA":
        # __train_pls_da(x_train, x_test, y_train, y_test)
        a = 5
    elif classifier == "LDA":
        __train_lda(x_train, x_test, y_train, y_test)
    elif classifier == "FFNET":
        __train_ffnet()


def test_classifier(classifier):
    if classifier == "PLS-DA":
        __test_pls_da()
    elif classifier == "LDA":
        __test_lda()
    elif classifier == "FFNET":
        __test_ffnet()


def train_pls_da(x_train_1, y_train_1, x_train_2, y_train_2, x_train_3, y_train_3, x_test, y_test):

    #select_nr_lv(x_train_1, y_train_1, "1vsA")
    #select_nr_lv(x_train_2, y_train_2, "2vsA")
    #select_nr_lv(x_train_3, y_train_3, "3vsA")

    clf1vsa = PLSRegression(n_components=3, scale=False)
    clf2vsa = PLSRegression(n_components=4, scale=False)
    clf3vsa = PLSRegression(n_components=5, scale=False)

    clf1vsa.fit(x_train_1, y_train_1)
    clf2vsa.fit(x_train_2, y_train_2)
    clf3vsa.fit(x_train_3, y_train_3)

    y_pred_1 = clf1vsa.predict(x_test)
    y_pred_2 = clf2vsa.predict(x_test)
    y_pred_3 = clf3vsa.predict(x_test)

    y_pred_tot = pd.Series(np.zeros(y_pred_1.__len__()))

    for elem in range(y_pred_1.__len__()):
        massimo = max(y_pred_1[elem], y_pred_2[elem], y_pred_3[elem])
        if y_pred_1[elem] == massimo:
            y_pred_tot[elem] = 1
        elif y_pred_2[elem] == massimo:
            y_pred_tot[elem] = 2
        elif y_pred_3[elem] == massimo:
            y_pred_tot[elem] = 3
        else:
            print("Couldn't find exact match, resorting to non demented")
            y_pred_tot[elem] = 1

    y_pred_tr1 = clf1vsa.predict(x_train_1)
    y_pred_tr2 = clf2vsa.predict(x_train_2)
    y_pred_tr3 = clf3vsa.predict(x_train_3)


    print("Train1 score:", accuracy_score(y_train_1, np.around(y_pred_tr1)))
    print("Train2 score:", accuracy_score(y_train_2, np.around(y_pred_tr2)))
    print("Train3 score:", accuracy_score(y_train_3, np.around(y_pred_tr3)))
    print("Test score:", accuracy_score(y_test, y_pred_tot))

    print(confusion_matrix(y_test, y_pred_tot))
    print(classification_report(y_test, y_pred_tot))

    confusion_matrix_nice(y_test, y_pred_tot)
    scores_plot(clf1vsa, x_train_1, y_train_1, "1vsA")
    scores_plot(clf2vsa, x_train_2, y_train_2, "2vsA")
    scores_plot(clf3vsa, x_train_3, y_train_3, "3vsA")

    # clf = PLSRegression(n_components=2, scale=False)
    # clf.fit(x_train, y_train)
    # y_pred = clf.predict(x_test)
    # y_pred = np.around(y_pred)


    #print(roc_auc_score(y_test, y_pred))


def select_nr_lv(X, y, title):
    # define cross-validation method
    cv = RepeatedKFold(n_splits=10, n_repeats=3, random_state=1)

    mse = []
    n = len(X)

    # Calculate MSE with only the intercept
    score = -1 * cross_val_score(PLSRegression(n_components=1), np.ones((n, 1)), y, cv=cv, scoring='neg_mean_squared_error').mean()
    mse.append(score)

    # Calculate MSE using cross-validation, adding one component at a time
    for i in np.arange(1, 20):
        pls = PLSRegression(n_components=i)
        score = -1 * cross_val_score(pls, X, y, cv=cv, scoring='neg_mean_squared_error').mean()
        mse.append(score)

    # plot test MSE vs. number of components
    plt.figure()
    plt.plot(mse)
    plt.xlabel('Number of PLS Components')
    plt.ylabel('MSE')
    plt.title(title)


def scores_plot(clf, x, y, title):

    scores = pd.DataFrame(clf.x_scores_)
    scores.index = x.T.columns

    colormap = {
        0: '#ff0000',  # Red
        1: '#0000ff',  # Blue
    }
    colorlist = [colormap[c] for c in y]


    #fig, ax = plt.subplots(figsize=(6, 6), dpi=80, facecolor='w', edgecolor='k')

    ax = scores.plot(x=0, y=1, kind='scatter', s=50, alpha=0.7, c=colorlist)
    plt.xlabel('Scores on LV 1')
    plt.ylabel('Scores on LV 2')
    plt.title(title)


def confusion_matrix_nice(y_test, y_pred):
    # Plot the confusion matrix to understand the classification in detail

    # pred_ls = np.argmax(y_pred, axis=1)
    # test_ls = np.argmax(y_test, axis=1)
    pred_ls = y_pred
    test_ls = y_test

    conf_arr = confusion_matrix(test_ls, pred_ls)

    fig, ax = plt.subplots(figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')
    plt.title('Alzheimer\'s Disease Diagnosis ')
    plt.xlabel('Prediction')
    plt.ylabel('Truth')
    # plt.show(ax)
    ax = sns.heatmap(conf_arr, cmap='Greens', annot=True, fmt='d', xticklabels=CLASSES, yticklabels=CLASSES)


def scatter_plot(y_train, data_projected):
    # PLot the transformed data
    markers = ['s', 'x', 'o']
    colors = ['r', 'g', 'b']

    fig = plt.figure(figsize=(10, 10))
    ax0 = fig.add_subplot(111)

    for l, m, c in zip(np.unique(y_train), markers, colors):
        ax0.scatter(data_projected[:, 0][y_train == l], data_projected[:, 1][y_train == l], c=c, marker=m)


def __train_lda(X_train, X_test, y_train, y_test):

    # define model
    lda = LinearDiscriminantAnalysis(n_components=2)

    # define model evaluation method
    cv = RepeatedStratifiedKFold(n_splits=10, n_repeats=3, random_state=1)

    # evaluate model
    scores = cross_val_score(lda, X_train, y_train, scoring='accuracy', cv=cv, n_jobs=-1)

    # summarize result
    print('Mean Accuracy: %.3f (%.3f)' % (np.mean(scores), np.std(scores)))

    data_projected_train = lda.fit_transform(X_train, y_train)
    data_projected_validation = lda.fit_transform(X_test, y_test)
    #data_projected_test = lda.fit_transform(x_test_opt, y_test_opt)

    scatter_plot(y_train, data_projected_train)
    scatter_plot(y_test, data_projected_validation)
    #scatter_plot(y_test_opt, data_projected_test)

    lda.fit(X_train, y_train)
    # x_test_r2=lda.transform(X_test)


    # Accuracy Score
    y_train_res = lda.predict(X_train)
    y_pred = lda.predict(X_test)
    #y_pred_opt = lda.predict(x_test_opt)

    print("Predicted\n", y_pred)
    print("Actual\n", y_test)
    print("Train score:", accuracy_score(y_train, y_train_res))
    print("Test score:", accuracy_score(y_test, y_pred))
    confusion_matrix_nice(y_test, y_pred)
    #print("Test score:", accuracy_score(y_test_opt, y_pred_opt), "\n")


def __train_ffnet():
    a = 5


def __test_pls_da():
    a = 5


def __test_lda():
    a = 5


def __test_ffnet():
    a = 5

